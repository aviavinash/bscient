import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '../app/pages/home/home.component'

export const AppRoutes: Routes = [
{ path: '', redirectTo: 'Home', pathMatch: 'full' },
{ path: 'Home',  component: HomeComponent }

];

export const AppRouteRoot = RouterModule.forRoot(AppRoutes, { useHash: true });
